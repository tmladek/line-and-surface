export function rotate(x: number, y: number, cx: number, cy: number, angleDegrees: number): [number, number] {
    const angleRad = (Math.PI / 180) * angleDegrees * -1;
    const cos = Math.cos(angleRad);
    const sin = Math.sin(angleRad);
    const nx = (cos * (x - cx)) + (sin * (y - cy)) + cx;
    const ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;
    return [nx, ny];
}
