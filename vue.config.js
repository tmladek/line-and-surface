module.exports = {
    publicPath: process.env.VUE_APP_BASE_URL || '/las/',
    devServer: {
        hot: false
    }
};
